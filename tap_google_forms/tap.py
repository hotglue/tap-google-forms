"""GoogleForms tap class."""

from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th  # JSON schema typing helpers
# TODO: Import your custom stream types here:
from tap_google_forms.streams import (
    GoogleFormsStream,
    ResponsesStream,
   
)
# TODO: Compile a list of custom stream types here
#       OR rewrite discover_streams() below with your custom logic.
STREAM_TYPES = [
    ResponsesStream,
   
]


class TapGoogleForms(Tap):
    """GoogleForms tap class."""
    name = "tap-google-forms"

    # TODO: Update this section with the actual config values you expect:
    config_jsonschema = th.PropertiesList(
        th.Property(
            "client_id",
            th.StringType,
            required=True,
        ),
        th.Property(
            "client_secret",
            th.StringType,
            required=True,
        ),
        th.Property(
            "refresh_token",
            th.StringType,
            required=True,
        ),
        th.Property(
            "form_id",
            th.StringType,
            required=True,
        ),
        th.Property(
            "api_url",
            th.StringType,
            default="https://forms.googleapis.com/v1",
            description="The url for the API service"
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == '__main__':
    TapGoogleForms.cli()
