"""Stream type classes for tap-google-forms."""

from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable
import copy
from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_google_forms.client import GoogleFormsStream

# TODO: Delete this is if not using json files for schema definition
SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")
# TODO: - Override `UsersStream` and `GroupsStream` with your own stream definition.
#       - Copy-paste as many times as needed to create multiple stream types.


class ResponsesStream(GoogleFormsStream):
    """Define custom stream."""
    name = "responses"
    path = "/forms"
    primary_keys = ["responseId"]
    records_jsonpath = "$.responses[*]"  # Or override `parse_response`.
    replication_key = "lastSubmittedTime"
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"
    schema = th.PropertiesList(
        th.Property("responseId", th.StringType),
        th.Property("createTime", th.DateTimeType),
        th.Property("lastSubmittedTime", th.DateTimeType),
        th.Property("answers", th.CustomType({"type": ["object", "string"]}))
        
    ).to_dict()
    def get_url(self, context: Optional[dict]) -> str:
        """Get stream entity URL.

        Developers override this method to perform dynamic URL generation.

        Args:
            context: Stream partition or context dictionary.

        Returns:
            A URL, optionally targeted to a specific partition or context.
        """
        url = "".join([self.url_base, self.path or ""])
        vals = copy.copy(dict(self.config))
        vals.update(context or {})
        for k, v in vals.items():
            search_text = "".join(["{", k, "}"])
            if search_text in url:
                url = url.replace(search_text, self._url_encode(v))
        return f"{url}/{self.config.get('form_id')}/responses"
